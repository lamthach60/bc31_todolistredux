import { arrTheme } from "../../JSS_StyledComponents/Theme/ThemeManager";
import { ToDoListDarkTheme } from "../../JSS_StyledComponents/Theme/TodoListDarkTheme";
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
} from "../types/ToDoListType";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    {
      id: "task-1",
      taskName: "task 1",
      done: true,
    },
    { id: "task-2", taskName: "task 2", done: false },
    { id: "task-3", taskName: "task 3", done: true },
    { id: "task-4", taskName: "task 4", done: false },
    { id: "task-5", taskName: "task 5", done: true },
  ],
  taskEdit: {
    id: "task-2",
    taskName: "task 2",
    done: false,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK: {
      if (action.newTask.taskName.trim() === "") {
        alert("Nhập vào task name");
        return { ...state };
      }
      //Kiểm tra tồn tại
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );
      if (index !== -1) {
        alert("Không được trùng task name");
        return { ...state };
      }
      taskListUpdate.push(action.newTask);
      //Xử lý xong thì gắn tasklist mới vào tasklist
      state.taskList = taskListUpdate;
      return { ...state };
    }
    case CHANGE_THEME: {
      let theme = arrTheme.find((theme) => theme.id == action.themeID);
      if (theme) {
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    case DONE_TASK: {
      //click vào button  check => dispatch  lên action có taskID
      let taskListUpdate = [...state.taskList];
      // Từ task id tìm ra task đó ở vị trí nào trong mảng tiến hành cập nhật lại thuộc tính done == true và cập nhật lại state của redux
      let index = taskListUpdate.findIndex((task) => task.id === action.taskID);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }
      //state.taskList = taskListUpdate
      return { ...state, taskList: taskListUpdate };
    }
    case DELETE_TASK: {
      // let taskListUpdate = [...state.taskList];
      // taskListUpdate = taskListUpdate.filter(
      //   (task) => task.id !== action.taskID
      // );

      // return { ...state, taskList: taskListUpdate };
      return {
        ...state,
        taskList: state.taskList.filter((task) => task.id !== action.taskID),
      };
    }
    case EDIT_TASK: {
      return { ...state, taskEdit: action.task };
    }
    default:
      return state;
  }
};
