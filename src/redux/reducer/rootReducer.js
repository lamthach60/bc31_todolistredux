import { combineReducers } from "redux";
import ToDoList_Reducer from "./ToDoList_Reducer";
export const rootReducer = combineReducers({
  ToDoList_Reducer,
});
