import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
} from "../types/ToDoListType";

export const addTaskAction = (newTask) => ({
  type: ADD_TASK,
  newTask,
});

export const changeThemeAction = (themeID) => ({
  type: CHANGE_THEME,
  themeID,
});
export const actionDoneTask = (taskID) => ({
  type: DONE_TASK,
  taskID,
});
//{return }=>({})
export const actionDeleteTask = (taskID) => ({
  type: DELETE_TASK,
  taskID,
});

export const actionEditTask = (task) => ({
  type: EDIT_TASK,
  task,
});
