import logo from "./logo.svg";
import "./App.css";
import DemoJSS from "./JSS_StyledComponents/DemoJSS/DemoJSS";
import { DemoTheme } from "./JSS_StyledComponents/Theme/DemoTheme";
import ToDoList from "./JSS_StyledComponents/BaitapStyleComponent/BaitapToDoList/ToDoList";

function App() {
  return (
    <>
      <ToDoList />
    </>
  );
}

export default App;
