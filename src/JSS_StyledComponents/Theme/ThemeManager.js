import { ToDoListDarkTheme } from "./TodoListDarkTheme";
import { ToDoListLightTheme } from "./TodoListLightTheme";
import { ToDoListPrimaryTheme } from "./TodoListPrimaryTheme";

export const arrTheme = [
  {
    id: 1,
    name: "Dark Theme ",
    theme: ToDoListDarkTheme,
  },
  {
    id: 2,
    name: "Light Theme",
    theme: ToDoListLightTheme,
  },
  {
    id: 3,
    name: "Primary Theme",
    theme: ToDoListPrimaryTheme,
  },
];
