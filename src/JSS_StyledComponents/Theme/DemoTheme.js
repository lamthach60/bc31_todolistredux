import styled, { ThemeProvider } from "styled-components";
import React, { Component } from "react";
const configTheme = {
  background: "#000",
  color: "red",
  fontSize: "20px",
  fontWeight: "bold",
};
const configLightTheme = {
  background: "yellow",
  color: "orange",
  fontSize: "20px",
  fontWeight: "bold",
};
export class DemoTheme extends Component {
  state = {
    currentTheme: configTheme,
  };
  handleChangeTheme = (event) => {
    this.setState({
      currentTheme: event.target.value == "1" ? configTheme : configLightTheme,
    });
  };
  render() {
    const DivStyle = styled.div`
      color: ${(props) => props.theme.background};
      padding: 5%;
      background-color: ${(props) => props.theme.color};
      font-size: ${(props) => props.theme.fontSize};
      font-weight: ${(props) => props.theme.fontWeight};
    `;

    return (
      <ThemeProvider theme={this.state.currentTheme}>
        <DivStyle>123</DivStyle>
        <select onChange={this.handleChangeTheme}>
          <option value="1">Dark Theme</option>
          <option value="2">Light Theme</option>
        </select>
      </ThemeProvider>
    );
  }
}
