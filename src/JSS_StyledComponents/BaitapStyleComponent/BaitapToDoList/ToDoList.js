import React, { Component } from "react";
import { Container } from "../../ComponentToDoList/Container";
import { ThemeProvider } from "styled-components";
import { ToDoListDarkTheme } from "../../Theme/TodoListDarkTheme";
import { ToDoListLightTheme } from "../../Theme/TodoListLightTheme";
import { ToDoListPrimaryTheme } from "../../Theme/TodoListPrimaryTheme";
import { Dropdown } from "../../ComponentToDoList/Dropdown";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../../ComponentToDoList/Heading";
import { TextField, Input, Label } from "../../ComponentToDoList/TextField";
import { Button } from "../../ComponentToDoList/Button";
import { Table, Tr, Td, Th, Thead, Tbody } from "../../ComponentToDoList/Table";
import { connect } from "react-redux";
import ToDoList_Reducer from "../../../redux/reducer/ToDoList_Reducer";
import {
  actionDeleteTask,
  actionDoneTask,
  actionEditTask,
  addTaskAction,
} from "../../../redux/action/ToDoListAction";
import { arrTheme } from "../../Theme/ThemeManager";
import { CHANGE_THEME, EDIT_TASK } from "../../../redux/types/ToDoListType";

class ToDoList extends Component {
  state = {
    taskName: "",
  };

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th className="align-middle">{task.taskName}</Th>
            <Th className="text-right align-middle">
              <Button
                onClick={() => {
                  this.props.dispatch(actionEditTask(task));
                }}
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(actionDoneTask(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(actionDeleteTask(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskComplete = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th className="align-middle">{task.taskName}</Th>
            <Th className="text-right align-middle">
              <Button
                onClick={() => {
                  this.props.dispatch(actionDeleteTask(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };
  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <div>
          <Container className="w-50">
            <Dropdown
              onChange={(e) => {
                let { value } = e.target;
                this.props.dispatch({
                  type: CHANGE_THEME,
                  themeID: value,
                });
              }}
            >
              {this.renderTheme()}
            </Dropdown>
            <Heading3>To Do List</Heading3>
            <TextField
              value={this.props.taskEdit.taskName}
              onChange={(e) => {
                this.setState({
                  taskName: e.target.value,
                });
              }}
              name="taskName"
              label="Task Name:"
              className="w-50"
            />
            <Button
              onClick={() => {
                //lấy thông tin người dùng nhập vào từ input
                let { taskName } = this.state;
                //tạo ra 1 task object
                let newTask = {
                  id: Date.now(),
                  taskName: taskName,
                  doneTask: false,
                };

                //đưa task lên redux thông qua phương thức dispatch
                this.props.dispatch(addTaskAction(newTask));
              }}
              className="ml-2"
            >
              <i className="fa fa-plus"></i> Add Task
            </Button>
            <Button className="ml-2">
              <i className="fa fa-upload"></i> Update
            </Button>
            <hr></hr>
            <Heading3>Task To Do</Heading3>
            <Table>
              <Thead>{this.renderTaskToDo()}</Thead>
            </Table>
            <Heading3>Task Completed</Heading3>
            <Table>
              <Thead>{this.renderTaskComplete()}</Thead>
            </Table>
          </Container>
        </div>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoList_Reducer.themeToDoList,
    taskList: state.ToDoList_Reducer.taskList,
    taskEdit: state.ToDoList_Reducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
