import React, { Component } from "react";
import { Button } from "../Component/Button";
import { StyledLink } from "../Component/Link";
import { TextField } from "../Component/TextField";

export default class DemoJSS extends Component {
  render() {
    return (
      <div>
        <Button className="button_style" primary fontSize2x>
          Hi Lâm
        </Button>
        <StyledLink id="123">a</StyledLink>
        <TextField inputColor="purple"></TextField>
      </div>
    );
  }
}
