import styled from "styled-components";
export const Button = styled.button`
  color: white;
  background: ${(props) => (props.primary ? "blue" : "orange")};
  font-weight: bold;
  font-size: ${(props) => (props.fontSize2x ? "2rem" : "1rem")};
  &:hover {
    color: black;
    background-color: white;
    transition: 1s;
    opacity: 0.5;
  }
`;
